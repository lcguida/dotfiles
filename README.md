# My Dotfiles

* zsh configs
* base16-tomorrow color scheme to VIM 
* Rubygems config
* Basic Git Configs

## Using Atom package-file

Generate the list (to update the file)

```bash
apm list --installed --bare > /path/to/package-list.txt
```

Install packages from list:

```bash
apm install --packages-file /path/topackage-list.txt
```
